import styles from './search.module.css'
import Input from "../../design_system/input/input";
import Select from "../../design_system/select/select";
import Button from "../../design_system/button/Button";
import {useState} from "react";
import axios from "axios";
import ReactLoading from 'react-loading';

const Search  = () => {

    const [select, setSelect] = useState('CLIENT_INFO')
    const [input, setInput] = useState('')
    const [loading, setLoading] = useState(false)
    const [result, setResult] = useState(null)

    const search = () => {
        setLoading(true)
        setResult(null)
        axios.post(`http://localhost:8000/${select}`,{
            id: select === "LIBELLE_INFO" ? input : parseInt(input)
        })
            .then((res) => {
                console.log(res.data)
            })
            .catch((err) => console.log(err))
            .finally(() => {
                setLoading(false)
            })

    }

    return(
        <div className={styles.container}>
            <h1>SEARCH</h1>
            <div className={styles.header}>
                <Select
                    value={select}
                    options={[
                        {
                            name : 'Users',
                            value: 'CLIENT_INFO'
                        },
                        {
                            name : 'Product',
                            value: 'LIBELLE_INFO'
                        }
                    ]}
                    onChange={setSelect}
                />
                <Input
                    containerClassName={styles.input}
                    className={styles.innerInput}
                    value={input}
                    onChange={(v) =>setInput(v.target.value)}
                />
                {loading ?
                    <ReactLoading type={"spin"} color={"#245FAD"} height={50} width={50} />
                    :
                    (
                        <Button
                            onClick={search}
                        >Search</Button>
                    )}
            </div>



        </div>
    )
}



export default Search;