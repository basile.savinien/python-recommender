import styles from './predict.module.css'
import Input from "../../design_system/input/input";
import Select from "../../design_system/select/select";
import Button from "../../design_system/button/Button";
import {useState} from "react";
import axios from "axios";
import ReactLoading from 'react-loading';

const Predict  = () => {

    const [select, setSelect] = useState('users_sickit')
    const [input, setInput] = useState('')
    const [loading, setLoading] = useState(false)
    const [result, setResult] = useState(null)

    const search = () => {
        setLoading(true)
        setResult(null)
        axios.post(`http://localhost:5000/${select}`,{
                id: select === "products" ? input : parseInt(input)
        })
            .then((res) => {

                setResult( typeof res.data?.data === "string" ?   JSON.parse(res.data?.data) : res.data?.data?.items);
            })
            .catch((err) => console.log(err))
            .finally(() => {
                setLoading(false)
            })

    }

    return(
        <div className={styles.container}>
            <h1>Prédictions</h1>
            <div className={styles.header}>
                <Select
                    value={select}
                    options={[
                        {
                            name : 'CLI_ID_SCIKIT',
                            value: 'users_sickit'
                        },
                        {
                            name : 'CLI_ID',
                            value: 'users'
                        },
                        {
                            name : 'LIBELLE',
                            value: 'products'
                        }
                    ]}
                    onChange={setSelect}
                />
                <Input
                    containerClassName={styles.input}
                    className={styles.innerInput}
                    value={input}
                    onChange={(v) =>setInput(v.target.value)}
                />
                {loading ?
                    <ReactLoading type={"spin"} color={"#245FAD"} height={50} width={50} />
                    :
                    (
                        <Button
                            onClick={search}
                        >Search</Button>
                    )}
            </div>
            <Result result={result} />


        </div>
    )
}

const Result = ({result}) => {
    if (!result || typeof result !== "object") return null
    return(
        <div style={{display: 'flex', flexDirection: 'column', marginTop: 20}}>
            <h2>Résult</h2>
            {
                typeof result === "object" && Object.values(result).sort((a,b) => {
                    const first = typeof a === "object" && (a.correlation || a.estimation)
                    const second = typeof b === "object" && (b.correlation || b.estimation)
                    if (!first || ! second) return null;
                    return a - b;
                }).map((v, index)=> {
                    if(typeof v === 'string') {
                        return(<span key={v} style={{marginTop:5 }}>- {v}</span>)
                    }
                    return(
                        <span key={v.LIBELLE}>{index + 1}- {v.LIBELLE || v.name}  (correlation: {v.correlation || v.estimation})</span>
                    );

                })}
        </div>
    )
}

export default Predict;