import styles from './graph.module.css'

import Select from "../../design_system/select/select";
import Button from "../../design_system/button/Button";
import {useState} from "react";
import axios from "axios";
import ReactLoading from 'react-loading';
import Chart from "react-apexcharts";

const column1 = ['MOIS_VENTE', 'MAILLE','FAMILLE']
const column2 = ['TICKET_ID', 'LIBELLE', 'CLI_ID', 'FAMILLE','MAILLE','MOIS_VENTE']

const chartOptions = {
        chart: {
            id: "basic-bar"
        }
}

const chartSeries =  [
    {
        name: "series-1",
        data: [30, 40, 45, 50, 49, 60, 70, 91]
    }
]

const Graph  = () => {

    const [selectType, setSelectType] = useState('groupby_distinct')
    const [selectC1, setSelectC1] = useState(column1[0])
    const [selectC2, setSelectC2] = useState(column2[1])
    const [loading, setLoading] = useState(false)
    const [result, setResult] = useState(null)

    const search = () => {
        setLoading(true)
        setResult(null)
        axios.get(`http://localhost:8000/${selectType}/${selectC1}/${selectC2}`)
            .then((res) => {
                const data = res.data;
                const key = Object.keys(data[0])[0];
                const value = Object.keys(data[0])[1];
                const chart = {
                    options : {
                        ...chartOptions,
                        xaxis: {
                            categories: data.map((r) => r[key])
                        }
                    },
                    series: [{
                        name: "count",
                        data: data.map((r) => r[value])
                    }]
                }
                setResult(chart)
            })
            .catch((err) => console.log(err))
            .finally(() => {
                setLoading(false)
            })

    }

    return(
        <div className={styles.container}>
            <h1>Graph</h1>
            <div className={styles.header}>
                <Select
                    value={selectType}
                    options={[
                        {
                            name : 'groupby_distinct',
                            value: 'groupby_distinct'
                        },
                        {
                            name : 'groupby',
                            value: 'groupby'
                        }
                    ]}
                    onChange={setSelectType}
                    containerClassName={styles.ctnSelect}
                />
                <Select
                    value={selectC1}
                    options={column1.filter((c) => c !== selectC2 ).map((c) => ({name: c, value:c}))}
                    onChange={setSelectC1}
                    containerClassName={styles.ctnSelect}
                />
                <Select
                    value={selectC2}
                    options={column2.filter((c) => c !== selectC1 ).map((c) => ({name: c, value:c}))}
                    onChange={setSelectC2}
                    containerClassName={styles.ctnSelect}
                />

                {loading ?
                    <ReactLoading type={"spin"} color={"#245FAD"} height={50} width={50} />
                    :
                    (
                        <Button
                            onClick={search}
                        >Show</Button>
                    )}
            </div>
            <Result result={result} />
        </div>
    )
}

const Result = ({result}) => {
    if (!result) return null
    return(
        <div style={{display: 'flex', flexDirection: 'column', marginTop: 80}}>
            <h2>Résult</h2>
            <Chart
                options={result.options}
                series={result.series}
                type="bar"
                width="800"
            />
        </div>
    )
}


export default Graph;