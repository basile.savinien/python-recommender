import logo from './logo.svg';
import './App.css';
import SideBar from "./components/side_bar/side_bar";
import {
    BrowserRouter as Router,
    Route,
    Routes
} from "react-router-dom";
import NavBar from "./components/nav_bar/nav_bar";
import Predict from './screen/predict/predict'
import Search from "./screen/search/search";
import Graph from "./screen/graph/graph";

function App() {
  return (
      <Router>
        <div className="app">
            <NavBar />
            <div className={"content"}>
                <SideBar />
                <Routes>
                    <Route path={'/predict'} element={<Predict />} />
                    <Route path={'/search'} element={<Search />} />
                    <Route path={'/graph'} element={<Graph />} />
                </Routes>


            </div>
        </div>
      </Router>
  );
}

export default App;
