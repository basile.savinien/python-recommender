import React from "react";
import styles from './link_row_styles.module.css'
import {Link} from 'react-router-dom'
import {useSpring, animated} from "react-spring";
import { useLocation } from "react-router-dom";
import classNames from "classnames";

const margin = 10;

const LinkRow = ({isOpen, name, slug, icon} ) => {
    const location = useLocation()
    const props = useSpring({ opacity: isOpen ? 1 : 0})

    const Icon = icon && React.cloneElement(icon, {
        height: 30,
        width: 30
    });
    const isSelected = location.pathname.replaceAll("/","") === slug;

    return(
        <Link to={slug} className={classNames(styles.container, isSelected && styles.selected, !isSelected && styles.notSelected)} style={{width:  `calc(100% - ${margin * 2}px) `, margin: `${margin}px ${margin}px 0`}}>
            <div className={styles.iconContainer} style={{width:  `calc(70px - ${margin * 2}px )`}}>
                {Icon}
            </div>
            <animated.div className={styles.nameContainer} style={{...props}}>
                <span className={styles.name}>{name}</span>
            </animated.div>
        </Link>
    )
}

export default LinkRow;
