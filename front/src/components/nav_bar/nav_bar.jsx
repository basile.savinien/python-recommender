import styles from "./nav_bar_styles.module.css";

const NavBar =  () => {
    return(
        <div className={styles.container} style={{height:80}}>
            <span className={styles.logo}>T-DAT</span>
        </div>
    );
}

export default NavBar;
