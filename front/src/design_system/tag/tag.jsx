import styles from './tag_styles.module.css';
import cn from 'classnames';
import {useCallback} from "react";

const Tag = ({name, index , containerClassName, textClassName, deleteCallback}) => {

    const deleteTag = useCallback((event) => {
        deleteCallback?.(event, index)
    },[index, deleteCallback])
    return(
        <div className={cn(styles.container, containerClassName)} >
            <span className={cn(styles.text, textClassName)}>{name}</span>
            <div className={styles.delete} onClick={deleteTag}>
                <span className={styles.delete_text}>+</span>
            </div>
        </div>
    )
}

export default Tag;
