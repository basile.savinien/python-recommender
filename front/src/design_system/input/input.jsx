import React, { useCallback, useRef, useState} from "react";
import styles from "./input_styles.module.css"
import classNames from "classnames";

const Input = ({title, containerClassName, color = "primary", titleClassName, className,...props}) => {
    const inputRef = useRef(null);
    const [isFocus, setIsFocus] = useState(false)

    const focus = useCallback(() => {
        console.log(inputRef)
        if (inputRef.current) {
            inputRef?.current?.focus()
        }
    },[inputRef.current])


    const onFocus = useCallback(() => {
        setIsFocus(true)
    },[])

    const onBlur = useCallback(() => {
        setIsFocus(false)
    },[])
    return(
        <div className={classNames(styles.container, containerClassName)}>
            {title && <span onClick={focus} className={classNames(styles.title,titleClassName, isFocus && styles[`titleFocus_${color}`])}>{title}</span>}
            <input
                onFocus={onFocus}
                onBlur={onBlur}
                className={classNames(styles.input,className, isFocus && styles[`inputFocus_${color}`])}
                ref={inputRef}
                {...props}
            />
        </div>

    )
}

export default Input
