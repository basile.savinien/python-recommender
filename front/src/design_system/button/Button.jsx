import React from "react";
import cn from "classnames"
import styles from './Button.module.css';

const Button = ({children, className, variant = "primary",...props}) => {
    return(
        <button  {...props} className={cn(styles.container,variant, className)}  >
            <span className={styles.text}>{children}</span>
        </button>
    )
}

export default Button;
