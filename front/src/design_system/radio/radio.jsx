import React from 'react';
import styles from './radio_styles.module.css';
import cn from "classnames";

const Radio = ({type, label, name, color = 'primary', ...props}) => {
    return (
        <>
            <div className={styles.container}>
                    <input id={name?.toString()} type={type} className={styles.radio}   {...props} />
                    <span className={cn(styles.check, color && styles[color])}/>
                    <label htmlFor={name?.toString()} className={styles.label}>{label}</label>
            </div>
        </>
    )
}

export default Radio;
