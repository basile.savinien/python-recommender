import React, { useEffect, useRef, useState} from "react";
import styles from './select_styles.module.css';
import {useSpring, animated} from "react-spring";
import cn from "classnames";



const Select = ({options, value, onChange, inputClassName, containerClassName, enableClickOutside = true}) => {
    const [isOpen, setIsOpen] = useState(false);
    const selectRef = useRef(null);
    const props = useSpring({
        opacity: isOpen ? 1 : 0 ,
        transform: isOpen ? 'translateY(5px)' : "translateY(25px)"
    })

    useEffect(() => {
        if (enableClickOutside) {
            document.addEventListener('click',handleClickOutside)
        }
        return () => {
            document.removeEventListener('click', handleClickOutside);

        }
    },[enableClickOutside, isOpen])

    const handleClickOutside = ( event) => {
        const {target} = event;
        if(!selectRef.current?.contains(target) && isOpen ) {
            toggleDropdown(false);
        }
    }

    const toggleDropdown = (value  = null ) => {
        setIsOpen((prevState => typeof value === 'boolean' ? value : !prevState));
    }

    const onChangeValue = (val, i) => {
        if (onChange) {
            onChange(val, i);
        }
        toggleDropdown(false);
    }

    return(
            <div className={cn(styles.select, containerClassName)} ref={selectRef}>
                <div className={cn(styles.input, inputClassName)} onClick={() => toggleDropdown()}>
                    <span>{options.find((option) => option.value === value)?.name}</span>
                    <div className={styles.icon}>{'>'}</div>
                </div>
                <animated.div className={cn(styles.dropdown, !isOpen && styles.close)} style={props} >
                    {
                        options.map(({value, name}, index) => {
                            return(
                                <div
                                    key={`${name}-${value}`}
                                    data-value={value}
                                    data-index={index}
                                    className={styles.optionContainer}
                                    onClick={() => onChangeValue(value, index)}
                                >
                                    <span className={styles.option}  >{name}</span>
                                </div>
                            )
                        })
                    }
                </animated.div>
            </div>
    )
}

export default Select;
