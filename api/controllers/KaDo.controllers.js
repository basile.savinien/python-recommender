const Product = require("../models/KaDo.model.js");

// Retrieve all Customers from the database.
exports.findAll = (req, res) => {
    Product.getAll((err, data) => {
        if (err)
          res.status(500).send({
            message:
              err.message || "Some error occurred while retrieving data."
          });
        else res.send(data);
      });
};

// Find a single Customer with a customerId
exports.findOne = (req, res) => {
    Product.findById(req.params.customerId, (err, data) => {
        if (err) {
          if (err.kind === "not_found") {
            res.status(404).send({
              message: `Not found CLI_ID with id ${req.params.customerId}.`
            });
          } else {
            res.status(500).send({
              message: "Error retrieving CLI_ID with id " + req.params.customerId
            });
          }
        } else res.send(data);
      });
};

exports.findGroupBy = (req, res) => {
    console.log("param", req.params)
    Product.findByGroup(req.params.index, req.params.tocount, (err, data) => {
        if (err) {
          if (err.kind === "not_found") {
            res.status(404).send({
              message: `Not found group by for index ${req.params.index}.`
            });
          } else {
            res.status(500).send({
              message: "Error retrieving index with id " + req.params.index
            });
          }
        } else res.send(data);
      });
};

exports.findGroupByNotDistinct = (req, res) => {
  console.log("param", req.params)
  Product.findByGroupNotDistinct(req.params.index, req.params.tocount, (err, data) => {
      if (err) {
        if (err.kind === "not_found") {
          res.status(404).send({
            message: `Not found group by for index ${req.params.index}.`
          });
        } else {
          res.status(500).send({
            message: "Error retrieving index with id " + req.params.index
          });
        }
      } else res.send(data);
    });
};

exports.findMax = (req, res) => {
  console.log("param", req.params)
  Product.findByMax((err, data) => {
      if (err) {
        if (err.kind === "not_found") {
          res.status(404).send({
            message: `Not found group by for index ${err}.`
          });
        } else {
          res.status(500).send({
            message: "Error retrieving index with id " + err
          });
        }
      } else res.send(data);
    });
};

exports.findMaxPerMonth = (req, res) => {
    console.log("param", req.params)
    Product.findByMonth(req.params.month, (err, data) => {
        if (err) {
            if (err.kind === "not_found") {
                res.status(404).send({
                    message: `Not found group by for month ${err}.`
                });
            } else {
                res.status(500).send({
                    message: "Error retrieving index with month " + err
                });
            }
        } else res.send(data);
    });
};

exports.findMaxPerClient = (req, res) => {
    console.log("param", req.params)
    Product.findMostBoughtByClient(req.params.client, (err, data) => {
        if (err) {
            if (err.kind === "not_found") {
                res.status(404).send({
                    message: `Not found group by for month ${err}.`
                });
            } else {
                res.status(500).send({
                    message: "Error retrieving index with month " + err
                });
            }
        } else res.send(data);
    });
};

exports.findMaxPerClientPerMonth = (req, res) => {
    //console.log("param", req.params)
    Product.findMostBoughtByClientByMonth(req.params.client, req.params.month, (err, data) => {
        if (err) {
            if (err.kind === "not_found") {
                res.status(404).send({
                    message: `Not found group by for month ${err}.`
                });
            } else {
                res.status(500).send({
                    message: "Error retrieving index with month " + err
                });
            }
        } else res.send(data);
    });
};

exports.findInfoByLibelle = (req, res) => {
  console.log("param", req.body.id)
  Product.findInfoByLibelle(req.body.id, (data, err) => {
      if (err) {
          if (err.kind === "not_found") {
              res.status(404).send({
                  message: `Not found group by for libelle ${err}.`
              });
          } else {
              res.status(500).send({
                  message: "Error retrieving index with libelle  " + err
              });
          }
      } else res.send(data);
  });
};

exports.findInfoByClient = (req, res) => {
  console.log("param", req.body.id)
  Product.findInfoByClient(req.body.id, (data, err) => {
      if (err) {
          if (err.kind === "not_found") {
              res.status(404).send({
                  message: `Not found group by for libelle ${err}.`
              });
          } else {
              res.status(500).send({
                  message: "Error retrieving index with libelle  " + err
              });
          }
      } else res.send(data);
  });
};