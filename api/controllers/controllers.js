var Product = require('../models/models.js');

exports.getLibelleNumberByUser = function (req, res, next) {
    let userid = parseInt(req.params.id)
  Product.aggregate([
    {$match:{'CLI_ID': userid}},
    {$group: {
        _id: '$CLI_ID',
        commandes: {$sum: 1}
    }}
    ], function(err, obj) {
        res.send(obj);
        console.log(obj);
    })
}

exports.getLibelleNumberByFamille = function (req, res, next) {
    //let familleid = parseInt(req.params.id)
  Product.aggregate([
    {$group: {
        _id: '$FAMILLE',
        commandes: {$sum: 1}
    }}
    ], function(err, obj) {
        res.send(obj);
        console.log(obj);
    })
}

exports.getByMailleByFamille = function (req, res, next) {
    //let familleid = parseInt(req.params.id)
  Product.aggregate([
    {$group: {
        _id: '$FAMILLE',
        Maille: "$MAILLE",
        commandes: {$sum: 1}
    }}
    ], function(err, obj) {
        res.send(obj);
        console.log(obj);
    })
}

exports.getByUser = function (req, res, next) {
    Product.find(
        {
            CLI_ID: req.params.id
        }, function(err, obj) {
            res.send(obj);
            console.log(obj);
        }
    );
}