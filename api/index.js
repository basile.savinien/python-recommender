const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors')
const app = express()
const port = 8000

// const router = require('./routes/routes.js')

// try {
//     mongoose.connect(process.env.MONGO_CONNECTION || 'mongodb://localhost:27017/test', {
//         useNewUrlParser: true,
//         useUnifiedTopology: true
//       }, () =>
//       console.log("connected"));
    
//   } catch (error) {
//     console.log("could not connect");
// }

// app.use(router);
app.use(cors())
app.get('/', function (req, res) {
  res.send('Hello World');
})

require("./routes/KaDo.routes.js")(app);

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
}) 