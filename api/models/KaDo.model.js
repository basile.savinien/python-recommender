const { promise } = require("../db/db.js");
const sql = require("../db/db.js");

// constructor
const Product = function(product) {
  this.CLI_ID = product.email;
  this.LIBELLE = product.libelle;
  this.MAILLE = product.maille;
  this.UNIVERS = product.libelle;
  this.FAMILLE = product.maille;
  this.PRIX_NET = product.prix_net;
  this.MOIS_VENTE = product.mois_vente;
  this.TICKET_ID = product.ticket_id;
};


Product.findById = (customerId, result) => {
  sql.query(`SELECT * FROM T_DAT.KaDo WHERE CLI_ID = ${customerId}`, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(err, null);
      return;
    }

    if (res.length) {
      console.log("found customer: ", res);
      result(null, res);
      return;
    }

    // not found Product with the id
    result({ kind: "not_found" }, null);
  });
};

Product.getAll = result => {
  sql.query("SELECT * FROM T_DAT.KaDo", (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }

    console.log("Products: ", res);
    result(null, res);
  });
};

Product.findByGroup = (index, tocount, result) => {
    sql.query(`SELECT ${index}, count(distinct ${tocount}) FROM T_DAT.KaDo group by ${index}`, (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(null, err);
        return;
      }
  
      console.log("Products: ", res);
      result(null, res);
    });
  };

Product.findByGroupNotDistinct = (index, tocount, result) => {
  sql.query(`SELECT ${index}, count(${tocount}) FROM T_DAT.KaDo group by ${index}`, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }

    console.log("Products: ", res);
    result(null, res);
  });
};

/*Product.findByMax = (result) => {
  sql.query(`SELECT FAMILLE, LIBELLE, NUMBER from T_DAT.Number_libelle`, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }

    console.log("Products: ", res);
    result(null, res);
  });
};*/

Product.findByMonth = (month, result) => {
  month = parseInt(month)
  sql.query(`SELECT LIBELLE, count(*) from T_DAT.KaDo where MOIS_VENTE = ${month} group by LIBELLE order by count(*) desc limit 20`, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }

    console.log("Products: ", res);
    result(null, res);
  });
}

Product.findMostBoughtByClient = (client, result) => {
  client = parseInt(client)
  sql.query(`SELECT LIBELLE, count(*) from T_DAT.KaDo where CLI_ID = ${client} group by LIBELLE order by count(*) desc limit 20`, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }

    result(null, res);
  });
}

Product.findMostBoughtByClientByMonth = (client, month, result) => {
  client = parseInt(client)
  month = parseInt(month)
  sql.query(`SELECT LIBELLE, MOIS_VENTE, count(*) from T_DAT.KaDo where CLI_ID = ${client} and MOIS_VENTE = ${month} group by LIBELLE, MOIS_VENTE order by count(*) desc limit 20`, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }

    result(null, res);
  });
}

Product.findInfoByLibelle = (libelle, result) => {
//console.log("libelle", libelle)
//console.log("type", typeof libelle)
  Promise.all([
    resultat_libelle(`select count(LIBELLE) from KaDo where LIBELLE = "${libelle}"`),
    resultat_libelle(`select count(distinct TICKET_ID) from KaDo where LIBELLE = "${libelle}"`),
    resultat_libelle(`select CLI_ID, LIBELLE, count(*) from KaDo where LIBELLE = "${libelle}" group by CLI_ID, LIBELLE order by count(*) desc limit 5`),
    resultat_libelle(`select TICKET_ID, LIBELLE, count(*) from KaDo where LIBELLE = "${libelle}" group by TICKET_ID, LIBELLE order by count(*) desc limit 5`)
  ]).then(res => {result(res)})
  
}

const resultat_libelle = (str) => {
  return new Promise ((resolve, reject) => {
    sql.query(str, (err, res) => {
      if (err) {
        console.log("error: ", err);
        reject(err)
      }
      resolve(res)
    });
  } ) 
}

Product.findInfoByClient = (client, result) => {
  console.log("libelle", client)
  console.log("type", typeof client)
    Promise.all([
      resultat_client(`select LIBELLE, count(LIBELLE) from KaDo where CLI_ID = ${client} group by LIBELLE order by count(LIBELLE) desc LIMIT 5`),
      resultat_client(`select TICKET_ID, count(LIBELLE) from KaDo where CLI_ID = ${client} group by TICKET_ID order by count(TICKET_ID) desc limit 5`),
      resultat_client(`select MOIS_VENTE, count(LIBELLE) from KaDo where CLI_ID = ${client} group by MOIS_VENTE order by count(LIBELLE) desc limit 1`),
      resultat_client(`select FAMILLE, count(LIBELLE) from KaDo where ${client} group by FAMILLE order by count(LIBELLE) desc limit 2`)
    ]).then(res => {result(res)})
    
  }
  
  const resultat_client = (str) => {
    return new Promise ((resolve, reject) => {
      sql.query(str, (err, res) => {
        if (err) {
          console.log("error: ", err);
          reject(err)
        }
        resolve(res)
      });
    } ) 
  }

module.exports = Product;