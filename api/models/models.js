var mongoose =  require('mongoose');

var ProductSchema = new mongoose.Schema({
    CLI_ID: {type: Number, required: true},
    FAMILLE: {type: String, required: true},
    LIBELLE: {type: String, required: true},
    MAILLE: {type: String, required: true},
    MOIS_VENTE: {type: Number, required: true},
    PRIX_NET: {type: Number, required: true},
    TICKET_ID: {type: Number, required: true},
    UNIVERS: {type: String, required: true},
  }, {
    collection: 'products'  
});

module.exports = mongoose.model('Product', ProductSchema);