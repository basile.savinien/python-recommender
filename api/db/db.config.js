const dotenv = require('dotenv');
dotenv.config();

/*module.exports = {
    HOST: "104.248.95.212",
    USER: "root",
    PASSWORD: "aDm1nistrator",
    DB: "T_DAT"
};*/

module.exports = {
    HOST: "localhost",
    USER: "root",
    PASSWORD: process.env.MYSQL_PASS,
    DB: "T_DAT"
};