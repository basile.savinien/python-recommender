const product = require("../controllers/KaDo.controllers.js");
var bodyParser = require('body-parser')
var jsonParser = bodyParser.json()
module.exports = app => {
    const product = require("../controllers/KaDo.controllers.js");
  
    // Retrieve all Customers
    app.get("/customers", product.findAll);
  
    // Retrieve a single Customer with customerId
    app.get("/customers/:customerId", product.findOne);

    // Group by distinct
    app.get("/groupby_distinct/:index/:tocount", product.findGroupBy);

    // Group by not distinct
    app.get("/groupby/:index/:tocount", product.findGroupByNotDistinct)

    // Select max number of specific libelle per famille
    app.get("/max_libelle_famille", product.findMax)

    // Select top number of specific libelle per month
    app.get("/LIBELLE/:month", product.findMaxPerMonth)

    // Select top number of libelle per specific client
    app.get("/CLIENT/:client", product.findMaxPerClient)

    // Select top number of libelle per specific client and specific month
    app.get("/CLIENT_MONTH/:client/:month", product.findMaxPerClientPerMonth)

    app.post("/LIBELLE_INFO", jsonParser, product.findInfoByLibelle)
    app.post("/CLIENT_INFO", jsonParser, product.findInfoByClient)
};