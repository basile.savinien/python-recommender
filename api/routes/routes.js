const Router = require('express').Router
const Controller = require('../controllers/controllers.js')
var Product = require('../models/models.js');

const router = Router();

router.get('/user/:id', Controller.getByUser);

router.get('/commandes/:id', Controller.getLibelleNumberByUser);

router.get('/familles', Controller.getLibelleNumberByFamille);

router.get('/familles_mailles', Controller.getByMailleByFamille);

module.exports = router;