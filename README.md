# Pré-requis

## Versions

* [Python 3.6.x](https://www.python.org/downloads/release/python-360/) est necessaire

* Derniere version de [Node](https://nodejs.org/en/download/)

* [Jupyter Notebook](https://jupyter.org/install)

## Dépendances 

Installation des dépendences python3

```python
pip3 install -r requirement.txt
```

Installation des dépendances node

```javascript
cd API
npm i
```

```javascript
cd FRONT
npm i
```

## Fichiers necessaires

Le dataset KaDo.csv accessible depuis gandalf à placer à la racine du projet

Le modèle Scikit déjà build accessible via le lien [Google Drive](https://drive.google.com/drive/folders/1A4aEu5GVthC0BnslmDG8qrjG48VWMDM6?usp=sharing) et à placer
dans le sous dossier recommender/

# recommender_2022_21

Dans le cadre du projet recommender, nous avons separé le rendu en trois parties.

## Le bootstrap

Le bootstrap est accessible via le dossier "bootstrap", il contient un petit projet se basant sur une base de données du Titanic. Ce sous-projet n'a pas été terminé
car nous avons prefere nous focus sur le projet principal.

## L'API et FRONT

Les dossiers API et FRONT contiennent le projet web, qui a pour objectif de pouvoir afficher des graphiques dynamique depuis la base de données. Pour lancer les deux
serveurs, il faudra lancer la commande

```javascript
npm run prod
```

dans les deux sous-projets. Le front est accessible à l'adresse localhost:3000 et le back localhost:8000

## Le recommender

Le recommender est le centre du projet, pour cela nous avons construit trois modèles de recommendation differents se basant sur deux algorithmes differents

* Pandas : utilisation de la fonction pivot de table de la librairie Pandas qui permet de determiner mathématiquement par correlation les produits recommendé selon les
autres produits et selon les autres user (deux routes API differentes).

* Scikit : Création d'un modèle de prédiction avec la librairie Scikit et l'algorithme SDVpp (Matrix Factorization-based algorithms). Le modèle peut etre directement
 construit via le notebook, mais il peut aussi directement être téléchargé depuis le Google Drive (voir ci-dessus) afin de gagner du temps.
 
* Server : ce notebook a pour unique objectif d'etre executé tout en une seule fois pour avoir toutes le serveur lancé
 
 Afin de lancer les projets, il suffit d'ouvrir jupyter notebook se nommant "server.ipynb" et de lancer toutes les cases une par une.
