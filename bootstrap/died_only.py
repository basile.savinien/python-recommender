import pandas as pd
import numpy as np

data = pd.read_csv('titanic.csv')

died = data.loc[data['survived'] == 0, 'survived'].count() #compte le nombre de morts
mean_age = data.loc[data['survived'] == 0].age.mean() #calcul moyenne d'age des morts
mean_price = data.loc[data['survived'] == 0].fare.mean() #calcul moyenne de prix des morts

print("Il y a", died, "survivants\nMoyenne d'age: ", round(mean_age, 3), "\nMoyenne de prix: ", round(mean_price, 3))