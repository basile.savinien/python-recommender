import pandas as pd
import numpy as np

data = pd.read_csv('titanic.csv') #On lie le fichier csv

data_surviving = data[data["survived"] == 1] #sous table regroupant les données des survivants
data_dead= data[data["survived"] == 0] #sous table regroupant les données des victimes

survived_age = data_surviving[["sex", "age"]].groupby("sex").mean() #moyennes d'age par sexe des survivants
death_age = data_dead[["sex", "age"]].groupby("sex").mean() #moyennes d'age par sexe des victimes

female_survivor = data_surviving.loc[data['sex'] == 'female', 'sex'].count() #nombre de survivants feminin
male_survivor = data_surviving.loc[data['sex'] == 'male', 'sex'].count() #nombre de survivants masculin


data_to_char = data #je crée une deuxieme table copié de la premiere
data_to_char['survived'] = np.where(data_to_char['survived'] == 1, 'survived', 'dead') #je modifie la nouvelle table avec 0=dead et 1=survived
survived_price = data_to_char[["survived", "fare"]].groupby("survived").mean() #creation de la moyenne du prix selon la survie

print("\nIl y a", female_survivor, "femmes survivantes et", male_survivor, "hommes survivants, soit :", round(((female_survivor*100)/data_surviving.shape[0]), 2), "% des survivants sont des femmes\n")
print("La moyenne d'age des hommes et femmes pour les survivants est:\n", survived_age)
print("\nLa moyenne d'age des hommes et femmes pour les victimes est:\n", death_age)
print("\nLa moyenne de prix depensé pour survivire est:", survived_price)