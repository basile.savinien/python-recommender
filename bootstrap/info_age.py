import pandas as pd
import numpy as np

data = pd.read_csv('titanic.csv')
data_10 = data.head(10)

null_value = data.isnull().sum().age #calcul du nombre de valeur null
not_null_value = data.shape[0]-data.isnull().sum().age #nombre de valeurs null - nombre de lignes totals

print("Il y a", null_value, "de valeur age null et", not_null_value, "de valeurs age non null")