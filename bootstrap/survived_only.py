import pandas as pd
import numpy as np

data = pd.read_csv('titanic.csv')

survivor = data.loc[data['survived'] == 1, 'survived'].sum() #calcul nombre de survivants
mean_age = data.loc[data['survived'] == 1].age.mean() #calcul moyenne d'age des survivants
mean_price = data.loc[data['survived'] == 1].fare.mean() #calcul moyenne de prix des survivants

print("Il y a", survivor, "survivants\nMoyenne d'age: ", round(mean_age, 3), "\nMoyenne de prix: ", round(mean_price, 3))